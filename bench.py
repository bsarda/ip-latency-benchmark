#!/usr/bin/env python3

# usage :
# launch the server
# python3 bench-server.py -s 192.168.54.26 --serverport 1300 -c 192.168.54.218 --clientport 1301 --number 2 -x
# launch the client
# python3 bench-server.py -s 192.168.54.26 --serverport 1300 -c 192.168.54.218 --clientport 1301 --number 2

import socket
import sys
import time
# import struct
import getopt
# import os

SOCKETTIMEOUT = 30         # 30 seconds timeout


# set global buffer size
class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg
        print >>sys.stderr, msg
        print >>sys.stderr, "for help use --help"


def usage():
    print("Usage:")
    print("--server | -s\t\tdefines the ip of the server (default = localhost)")
    print("--serverport | -p\t\tdefines the port to lsten (default = 8080)")
    print("--client | -c\t\tdefines the ip of the client (default = localhost)")
    print("--clientport | -q\t\tdefines the port to lsten (default = 8081)")
    print("--buffersize | -b\t\tsize of the buffer (default = 1024)")
    print("--number | -n\t\tcount of datagram to send (default = 100)")
    print("--cycles | -y\t\tcount of cycles of numbers (default = 10)")
    sys.exit()


def benchmark(server, serverport, client, clientport, buffersize, count, cycles):
    # client socket - to ping to the server
    server_address = (server, serverport)
    socket_sender = socket.socket(
                socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    socket_sender.connect(server_address)
    socket_sender.settimeout(SOCKETTIMEOUT)
    # client listener - to receive the server's reply
    client_address = (client, clientport)
    socket_echo = socket.socket(
                socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    socket_echo.bind(client_address)
    socket_echo.settimeout(SOCKETTIMEOUT)

    print("Benchmark UDP...")
    # init the total duration
    duration = 0.0
    # create the datagram to send
    b = bytes("a"*buffersize, "utf-8")
    try:
        for i in range(0, count):
            start = time.time()
            # send to the server
            socket_sender.sendall(b)
            data = socket_echo.recv(buffersize)
            duration += time.time() - start
            if data != b:
                print("Error: Not the same.", data, b)
                sys.exit(-2)
    except KeyboardInterrupt:
        print("Program interupted.")
        sys.exit(-1)
    except socket.timeout:
        print("The program has timed out.")
        return 0
    socket_sender.close()
    return (duration*pow(10, 6)/count)


def start_server(server, serverport, client, clientport, buffersize, count, cycles):
    # server socket - which receives from client
    server_address = (server, serverport)
    socket_receiver = socket.socket(
                socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    socket_receiver.bind(server_address)
    socket_receiver.settimeout(SOCKETTIMEOUT)
    # client socket - to pong to the client
    client_address = (client, clientport)
    socket_reply = socket.socket(
                socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    socket_reply.connect(client_address)
    socket_reply.settimeout(SOCKETTIMEOUT)

    print("UDP server is running...")
    try:
        while True:
            # receive data...
            data = socket_receiver.recv(buffersize)
            if not data:
                break
            # answer to source
            socket_reply.sendall(data)
    except KeyboardInterrupt:
        print("Program interupted.")
        pass
    except socket.timeout:
        print("The program has timed out.")
        pass
    socket_reply.close()


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        # get the arguments
        try:
            opts, args = getopt.getopt(
                    argv[1:],
                    "hs:v:p:v:c:v:q:v:b:v:n:v:y:v:x",
                    ["help","server=","serverport=","client=","clientport=",
                            "buffersize=","number=","cycles=","execsrv"])
        except getopt.GetoptError as msg:
            raise Usage(msg)

        # the default value for vars
        server = "localhost"
        client = "localhost"
        serverport = 8080
        clientport = 8081
        buffersize = 1024
        count = 100
        cycles = 10
        execsrv = False
        # switch on options
        for o, a in opts:
            if o in ("-x", "--execsrv"):
                execsrv = True
            elif o in ("-s", "--server"):
                server = a
            elif o in ("-p", "--serverport"):
                serverport = int(a)
            elif o in ("-c", "--client"):
                client = a
            elif o in ("-q", "--clientport"):
                clientport = int(a)
            elif o in ("-b", "--buffersize"):
                buffersize = int(a)
            elif o in ("-n", "--number"):
                count = int(a)
            elif o in ("-y", "--cycles"):
                cycles = int(a)
            elif o in ("-h", "--help"):
                # help on usage
                usage()
            else:
                assert False, "unhandled option"
        if execsrv:
            start_server(server, serverport,
                        client, clientport,
                        buffersize, count, cycles)
        else:
            average = 0.0
            passed_cycles = cycles
            for y in range(0, cycles):
                avg_measure += benchmark(server, serverport,
                                    client, clientport,
                                    buffersize, count, cycles)
                average += avg_measure
                if avg_measure == 0:
                    passed_cycles -= 1
            average = average/passed_cycles
            print("Average round-trip latency is  ",average, "us. Measured with UDP, in ",passed_cycles," cycles")
        # more code, unchanged
    except Usage as err:
        return 5

if __name__ == "__main__":
    sys.exit(main())
